
Highcharts.setOptions({
    chart: {
        style: {
            fontFamily: "'Pathway Gothic One', sans-serif;"
        }
    }
});


// Highcharts.chart('container', {
// colors: ['#029676','#c0cf3a','#8ab833','#549e39','#bbc3bd','#8f9191','#e3ded1'],

//     chart: {
//         type: 'bar',
//         backgroundColor: null
        

//     },
//     title: {
//         text: ''
//     },
//     xAxis: {
        
//         categories: ['Your WATT3 Estimated Savings','WATT3 Solar PV & Battery Savings', 'Basic Solar PV & Battery Savings',  'Solar PV Savings','Efficiency Savings'],
// gridLineWidth: 0,
//    tickLength: 0,
//    labels: {
//        enabled: false,
//         style: {
//          color: '#E0E0E3',
//          fontSize: '10px'
//       }
//    }

//     },
//     yAxis: {
//          enabled: true,
//          gridLineDashStyle: 'longdash',
//     gridLineWidth: 1,
//         min: 0,
//         max : 70,
//         title: {
//             x: -25,
//             text: 'Savings compared to standard monthly bill',
//             style: {
//                 color: '#E0E0E3'
//             }
//         },
//         labels: {
           
//             style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         }
        
//     },
//     tooltip :{
//         enabled: false
//     },
//     legend: {
//             enabled: false,
//                         style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//     },
//     plotOptions: {
//         bar:{
//             borderWidth:0
//         },
//         series: {
//             stacking: 'normal',
//             pointPadding: 0,
//             groupPadding: 0.1,
//             colorByPoint: true,
//             dataLabels: {
//                         borderWidth: 0,
//                         enabled: true,
//                         color: '#000',
//                         align: 'left',
//                         style: {fontWeight: 'bolder', textShadow: false,border: '#5e5e5e'},
//                         formatter: function() {return this.x},
//                         inside: true,
//                         textOutline: 0
                     
//                     },
//         }

//     },
//     series: [{
//         data: [67,52, 42, 23, 12]
//     }],
//             credits: {
//             enabled: false
//         },
//     navigation: {
//         buttonOptions: {
//             enabled: false
//         }
//     }        
        
// });

//     var xCategories =  ['Day 1', 'Year 1', 'Year 2','Year 3','Year 4','Year 5','Year 6'];
// Highcharts.chart('container2', {

//     colors: ['#029676','#c0cf3a','#8ab833','#549e39','#bbc3bd','#8f9191','#e3ded1'],
//     chart: {
//          backgroundColor: null,
//         type: 'area',
//         spacingBottom: 30
//     },
//     title: {
//         text: ''
//     },
//     xAxis: {
        
//             startOnTick: false,
//             endOnTick: false,
//             minPadding: 0,
//             maxPadding: 0,
            
        
//                 title: {
//             text: 'Datetime',
//              style: {
//             color: '#E0E0E3'}

//         },
//     labels: {
//         formatter: function() {
//         return xCategories[this.value]
//                 },
//             style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         }
//     },
//     yAxis: {
//         enabled: false,
//         gridLineWidth: 0,
//    tickLength: 0,
        
//         title: {
//             text: '',
//             style: {
//             color: '#E0E0E3'
//             }
//         },
//         labels: {
//             enabled:false,
//             style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         }
//     },
//     tooltip:{
//         enabled:false
//     },
//     plotOptions: {
//         area: {
//             fillOpacity: 0.5,
//             marker: {
//             enabled: false
//         },
//         lineWidth: 0
//     },
//     // series : {
//     // dataLabels :{
//     //                         borderWidth: 0,
//     //                     enabled: true,
//     //                     color: '#000',
//     //                     align: 'left',
//     //                     style: {fontWeight: 'bolder', textShadow: false,border: '#5e5e5e'},
//     //                     formatter: function() {return this.x},
//     //                     inside: true,
//     //                     textOutline: 0
//     // },
//     // },



//     },
//     credits: {
//         enabled: false
//     },
//         navigation: {
//         buttonOptions: {
//             enabled: false
//         }
//     },
//         legend: {
//                         itemStyle: {

//                  color: '#5e5e5e'
//               }
//     },

//     series: [{
//         name: 'Initial Investment Cost',
//         data: [100, 100,null,null,null,null]
//     }, {
//         name: 'Watt3 savings over lifetime',
//         data: [null,50, 50,50,50,50]
//     }]
// });

// Highcharts.chart('container3', {
//         colors: ['#029676','#549e39','#bbc3bd','#8f9191','#e3ded1'],
//     chart: {
//          backgroundColor: null,
//         type: 'area',
//         spacingBottom: 30
//     },
//     title: {
//         text: ''
//     },
// xAxis: {
        
//             startOnTick: false,
//             endOnTick: false,
//             minPadding: 0,
//             maxPadding: 0,
            
        
//                 title: {
//             text: 'Datetime',
//              style: {
//             color: '#E0E0E3'}

//         },
//     labels: {
//         formatter: function() {
//         return xCategories[this.value]
//                 },
//             style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         }
//     },
//     yAxis: {
//         gridLineWidth: 0,
//         enabled: false,
//         title: {
//             text: '',
//                     style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         },
//                 labels: {
//                     enabled: false,
//             style: {
//             color: '#E0E0E3',
//             fontSize: '10px'
//             }
//         }
//     },
//     tooltip:{
//         enabled:false
//     },
//     plotOptions: {
//         lineWidth: 0,
//         area: {
//             fillOpacity: 0.5,
                   
//     marker: {
//             enabled: false
//         }
//  },
//     },
//     credits: {
//         enabled: false
//     },
//             navigation: {
//         buttonOptions: {
//             enabled: false
//         }
//     },
//         legend: {
//                         itemStyle: {

//                  color: '#5e5e5e'
//               }
//     },

//     series: [{
//         name: 'Watt3 savings over lifetime',
//         data: [90,90, 90,90,90,90]
//     },{
//         name: 'Lifetime system investments',
//         data: [130,130,130,130,130,130]
//     }]
// });





Highcharts.chart('container4', {
colors: ['#029676','#bbc3bd','#bbc3bd','#bbc3bd','#bbc3bd','#8f9191','#e3ded1'],

    chart: {
        type: 'bar',
        backgroundColor: null
        

    },
    title: {
        text: ''
    },
    xAxis: {
        tickInterval: 10,

        
        categories: ['Your WATT3 Estimated Savings', 'Basic Solar PV & Battery Savings',  'Solar PV Savings','Efficiency Savings'],
gridLineWidth: 0,
   tickLength: 0,
   labels: {
       enabled: false,
        style: {
         color: '#E0E0E3',
         fontSize: '10px'
      }
   }

    },
    yAxis: {
        tickInterval: 5,
         enabled: true,
         gridLineColor: '#bbc3bd',
         gridLineDashStyle: 'longdash',
    gridLineWidth: 1,
        min: 0,
        max :45,
        title: {
            x: -25,
            text: 'Savings compared to standard monthly bill',
            style: {
                color: '#E0E0E3'
            }
        },
        labels: {
           
            style: {
            color: '#E0E0E3',
            fontSize: '10px'
            }
        }
        
    },
    tooltip :{
        enabled: false
    },
    legend: {
            enabled: false,
                        style: {
            color: '#E0E0E3',
            fontSize: '10px'
            }
    },
    plotOptions: {
        bar:{
            borderWidth:0
        },
        series: {
            stacking: 'normal',
            pointPadding: 0,
            groupPadding: 0.1,
            colorByPoint: true,
            dataLabels: {
                        borderWidth: 0,
                        enabled: true,
                        color: '#000',
                        align: 'left',
                        style: {fontWeight: 'bolder', textShadow: false,border: '#5e5e5e'},
                        formatter: function() {return this.x},
                        inside: true,
                        textOutline: 0
                     
                    },
        }

    },
    series: [{
        data: [ 42, 30, 23, 12]
    }],
            credits: {
            enabled: false
        },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    }        
        
});




        Highcharts.chart('bill', {
            colors: ['#029676','#bbc3bd','#bbc3bd','#bbc3bd','#bbc3bd','#8f9191','#e3ded1'],

            chart: {
                type: 'arearange',
                zoomType: 'x',
                backgroundColor: null
            },

            title: {
                text: ' '
            },

            xAxis: {
                
                title: {
                    align: 'high',
                text: 'Time ',
                            style: {

                textAlign:'right'
            }
            },
                gridLineWidth: 0,
                tickLength: 0,
                   labels: {
       enabled: false},

                min:0,
                lineColor: '#bbc3bd',
                lineWidth:1,
            },

            yAxis: {
                
                title: {
                    align: 'high',
                text: 'Costs',
                            style: {

                marginTop: 10
            }
        },
        lineColor: '#bbc3bd',
        lineWidth:1,
                gridLineWidth: 0,
                   labels: {
       enabled: false},

                min:0
            },

            tooltip: {
enabled: false
            },

            legend: {
                enabled: false
            },

            series: [{
                name: 'Savings of using battery',
                data: [[0,0],[241,344],[472,674],[692,989],
                [893,1276],[1074,1534],[1244,1777],[1415,2021],[1575,2250],[1756,2508],[1957,2795]
                
                
                ]
      
            }],
                credits: {
        enabled: false
    },
            navigation: {
        buttonOptions: {
            enabled: false
        }
    }
            
            
            



        }, function (chart) { // on complete

    chart.renderer.text('Bill with WATT3', 400, 280)
        .attr({
            rotation: -20
        })
        .css({
            color: '#000',
            fontSize: '16px'
        })
        .add();
        
         chart.renderer.text('Standard Bill', 400, 180)
        .attr({
            rotation: -20
        })
        .css({
            color: '#000',
            fontSize: '16px'
        })
        .add();
        
        chart.renderer.text('Savings', 540, 190)
        .attr({
            rotation: -30
        })
        .css({
            color: '#000',
            fontSize: '26px',
            zIndex: 10
        })
        .add();

});
 


  var ranges = [["Jan", 70, 100], ["Feb", 137, 196], ["Mar", 201, 288], ["Apr", 260, 371], ["May", 312, 446], ["Jun", 362, 517], ["Jul", 411, 588], ["Aug", 458, 654], ["Sep", 510, 729], ["Oct", 569, 813], ["Nov", 633, 904], ["Dec", 700, 1000]],
      bills_with_battery = [["Jan", 70], ["Feb", 137], ["Mar", 201], ["Apr", 260], ["May", 312], ["Jun", 362], ["Jul", 411], ["Aug", 458], ["Sep", 510], ["Oct", 569], ["Nov", 633], ["Dec", 700]],
      bills_without_battery = [["Jan", 100], ["Feb", 196], ["Mar", 288], ["Apr", 371], ["May", 446], ["Jun", 517], ["Jul", 588], ["Aug", 654], ["Sep", 729], ["Oct", 813], ["Nov", 904], ["Dec", 1000]];

  var energy_bills = Highcharts.chart('energy-bills', {
    colors: ['#029676','#549e39','#c0cf3a','#8ab833'],
        chart: {

        backgroundColor: null
    },
    title: {
      text: "Cumulative Energy Bill"
    },
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
    gridLineColor: '#bbc3bd',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      title: {
        text: "Value of bill [CHF]"
      },
      labels: {
        align: "left",
        x: 0,
        y: -3
      }
    },
    tooltip: {
        enabled: false
    },
    series: [{
      name: 'Bills with WATT3 platform',
      data: bills_with_battery,
      zIndex: 1,
      lineWidth: 3,
      color: "#0abe50"
    }, {
      name: 'Bills without WATT3 platform',
      data: bills_without_battery,
      zIndex: 1,
      lineWidth: 3
    }, {
      name: 'Range',
      data: ranges,
      type: 'arearange',
      lineWidth: 0,
      linkedTo: ':previous',
      color: "#0abe50",
      fillOpacity: 0.3,
      zIndex: 0
    }],
    credits: {
        enabled: false
    },
            navigation: {
        buttonOptions: {
            enabled: false
        }
    }
  });
