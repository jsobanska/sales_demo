
Highcharts.chart('gauge1', {

    chart: {
        type: 'solidgauge',
        marginTop: 50,
        backgroundColor: null
    },
    title: {
        text: 'Autoconsumption<br>80%',
        style: {
            fontSize: '24px'
        },
            verticalAlign: 'middle',
    floating: true
    },

    tooltip: {
    enabled: false
    },

    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ 
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: '#E0E79D'
        }]
    },

    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },
        credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },

    series: [{
        name: 'Autoconsumption',
        data: [{
            color: '#c0cf3a',
            radius: '112%',
            innerRadius: '88%',
            y: 80
        }]
    } ]
},

/**
 * In the chart load callback, add icons on top of the circular shapes
 */
function callback() {

    // Move icon
    // this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
    //     .attr({
    //         'stroke': '#303030',
    //         'stroke-linecap': 'round',
    //         'stroke-linejoin': 'round',
    //         'stroke-width': 2,
    //         'zIndex': 10
    //     })
    //     .translate(190, 26)
    //     .add(this.series[2].group);
});





Highcharts.chart('gauge2', {

    chart: {
        type: 'solidgauge',
        marginTop: 50,
        backgroundColor: null
    },
    title: {
        text: 'Demand Charges<br>30%',
        style: {
            fontSize: '24px'
        },
            verticalAlign: 'middle',
    floating: true
    },

    tooltip: {
    enabled: false
    },

    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ 
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: '#E0E79D'
        }]
    },

    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },
        credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },

    series: [{
        name: 'Demand Charges',
        data: [{
            color: '#c0cf3a',
            radius: '112%',
            innerRadius: '88%',
            y: 30
        }]
    } ]
},

/**
 * In the chart load callback, add icons on top of the circular shapes
 */
function callback() {

    // Move icon
    // this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
    //     .attr({
    //         'stroke': '#303030',
    //         'stroke-linecap': 'round',
    //         'stroke-linejoin': 'round',
    //         'stroke-width': 2,
    //         'zIndex': 10
    //     })
    //     .translate(190, 26)
    //     .add(this.series[2].group);
});




Highcharts.chart('gauge3', {

    chart: {
        type: 'solidgauge',
        marginTop: 50,
        backgroundColor: null
    },
    title: {
        text: 'Solar Productions<br>45%',
        style: {
            fontSize: '24px'
        },
            verticalAlign: 'middle',
    floating: true
    },

    tooltip: {
    enabled: false
    },

    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ 
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: '#E0E79D'
        }]
    },

    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },
        credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },

    series: [{
        name: 'Solar Production',
        data: [{
            color: '#c0cf3a',
            radius: '112%',
            innerRadius: '88%',
            y: 45
        }]
    } ]
},

/**
 * In the chart load callback, add icons on top of the circular shapes
 */
function callback() {

    // Move icon
    // this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
    //     .attr({
    //         'stroke': '#303030',
    //         'stroke-linecap': 'round',
    //         'stroke-linejoin': 'round',
    //         'stroke-width': 2,
    //         'zIndex': 10
    //     })
    //     .translate(190, 26)
    //     .add(this.series[2].group);
});



Highcharts.chart('gauge4', {

    chart: {
        type: 'solidgauge',
        marginTop: 50,
        backgroundColor: null
    },
    title: {
        text: 'Summer Costs<br>19%',
        style: {
            fontSize: '24px'
        },
            verticalAlign: 'middle',
    floating: true
    },

    tooltip: {
    enabled: false
    },

    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ 
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: '#E0E79D'
        }]
    },

    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },
        credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },

    series: [{
        name: 'Summer Costs',
        data: [{
            color: '#c0cf3a',
            radius: '112%',
            innerRadius: '88%',
            y: 19
        }]
    } ]
},

/**
 * In the chart load callback, add icons on top of the circular shapes
 */
function callback() {

    // Move icon
    // this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
    //     .attr({
    //         'stroke': '#303030',
    //         'stroke-linecap': 'round',
    //         'stroke-linejoin': 'round',
    //         'stroke-width': 2,
    //         'zIndex': 10
    //     })
    //     .translate(190, 26)
    //     .add(this.series[2].group);
});
