Highcharts.chart('halfpie', {
	colors: ['#029676','#c0cf3a','#8ab833'],
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        backgroundColor: null
    },
    title: {
        text: 'Before<br> investment',
        align: 'center',
        verticalAlign: 'middle',
        y: 40
    },
    tooltip: {
        enabled: false
    },
    plotOptions: {
        pie: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                distance: -50,
                align: 'center',
                style: {
                    fontWeight: 'bold',
                    color: 'white',
                    
                },
                          
                format: '<b>{point.name}:</b> {point.y} CHF'

            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%']
        }
    },
            credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: [{
        type: 'pie',
        name: '',
        innerSize: '50%',
        data: [
            ['Grid Bill',   2100]

        ]
    }]
});

Highcharts.chart('halfpie1', {
	colors: ['#029676','#c0cf3a','#8ab833'],
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        backgroundColor: null
    },
    title: {
        text: 'After<br> investment',
        align: 'center',
        verticalAlign: 'middle',
        y: 40
    },
    tooltip: {
        enabled: false
    },
    plotOptions: {
        pie: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                distance: -50,
                align: 'center',
                style: {
                    fontWeight: 'bold',
                    color: 'white',
                    
                },
                          
                format: '<b>{point.name}:</b> {point.y} CHF'

            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%']
        }
    },
            credits: {
        enabled: false
      },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: [{
        type: 'pie',
        name: '',
        innerSize: '50%',
        data: [
            ['Grid Bill',   700],
            ['PV',   1000],
            ['Battery ', 400]

        ]
    }]
});

